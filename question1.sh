#!/usr/bin/ksh

echo "Please enter a positive integer:"
read num
count=0
str=""
while [ $count -le $num ]
do
 out=0
 line=""
 while [ $out -le $count ]
 do
  line=$line"$out "
  out=$(( $out + 1 ))
 done
 str=$str"$line\n"
 count=$(($count+1))
done

echo $str
