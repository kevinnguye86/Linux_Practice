#!/usr/bin/ksh
echo "Please enter 10 numbers. Save even/odd to file"

for var in 1 2 3 4 5 6 7 8 9 10
do
read num
if [ $((num%2)) -eq 0 ] ; then
printf $num" " >> even.txt
else
printf $num" " >> odd.txt
fi
done
