#!/usr/bin/ksh
echo "Please enter 10 numbers. Print number of positive/negative numbers"

pos=0
neg=0

for var in 1 2 3 4 5 6 7 8 9 10
do
read num
 if [ $num -lt 0 ]
 then
 neg=$((neg+1))
 else
 pos=$((pos+1))
 fi
done
echo Number of positive numbers: $pos
echo Number of negative numbers: $neg
