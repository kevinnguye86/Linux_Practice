#!/usr/bin/ksh

echo "Please enter the height of the tower:"
read h
count=$(($h-1))
str=""

while [ $count -ge 0 ]
do
  line=""
  subcount=0
  while [ $subcount -lt $h ]
  do
    if [ $subcount -lt $count ]
    then
      line=$line" "
    else
      line=$line"* "
    fi
    subcount=$(($subcount+1))
  done
  str=$str"$line\n"
  count=$(($count-1))
done

echo "$str"
