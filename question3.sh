echo "Please enter a number to find factorials:"
read num
fact=1
while [ $fact -le $num ]
do
  if [ $((num%fact)) -eq 0 ]
  then
  echo $fact
  fi
  fact=$(($fact+1))
done
