#!/usr/bin/ksh

echo -n "Enter a number:"
read Input_Number

if [ $Input_Number -eq 1 ]
then
echo "$Input_Number is neither prime nor composite number"
exit 0;
fi

Num_Factors=`factor $Input_Number|wc -w`

if [ $Num_Factors -eq 2 ]
then
echo "$Input_Number is a prime number"
else
echo "$Input_Number is not a prime number"
fi
